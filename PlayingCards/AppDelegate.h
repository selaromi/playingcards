//
//  AppDelegate.h
//  PlayingCards
//
//  Created by Ignacio Morales on 11/05/14.
//  Copyright (c) 2014 Ignacio Morales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
