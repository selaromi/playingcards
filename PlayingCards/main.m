//
//  main.m
//  PlayingCards
//
//  Created by Ignacio Morales on 11/05/14.
//  Copyright (c) 2014 Ignacio Morales. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
